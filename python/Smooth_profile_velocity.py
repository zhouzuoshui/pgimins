#!/usr/bin/python3

import numpy as np
import math

from Read_profile import *
from Write_profile import *
from utils import deg_to_rad, rad_to_deg

def Smooth_profile_velocity(in_filename, out_filename):
    # Smooth_profile_velocity - Adjusts the velocity in a motion profile file to
    # remove jitter resulting from numerical rounding in the position input to
    # Adjust_profile_velocity
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 2/4/2012 by Paul Groves
    #
    # Inputs
    #  in_filename     Name of inpit file, e.g. 'In_profile.csv'
    #  out_filename    Name of inpit file, e.g. 'Out_profile.csv'

    # Begins

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Input truth motion profile from .csv format file
    [in_profile, no_epochs, ok] = Read_profile(in_filename)

    # End script if there is a problem with the file
    if not ok:
        return

    # First (epoch
    out_profile = np.matrix(np.zeros((no_epochs-1, 10)))
    out_profile[0, :] = in_profile[0, :]

    # Main loop
    for epoch in range(1, no_epochs-1):

        # Input data from profile
        time = in_profile[epoch, 0]
        L_b = in_profile[epoch, 1]
        lambda_b = in_profile[epoch, 2]
        h_b = in_profile[epoch, 3]
        v_eb_n_current = in_profile[epoch, 4:7].transpose()
        eul_nb = in_profile[epoch, 7:10].transpose()
        v_eb_n_prev = in_profile[epoch - 1, 4:7].transpose()
        v_eb_n_next = in_profile[epoch + 1, 4:7].transpose()

        # Smooth velocity
        v_eb_n = 0.5 * v_eb_n_current + 0.25 * v_eb_n_prev + 0.25 * v_eb_n_next

        # Generate output profile record
        out_profile[epoch, 0] = time
        out_profile[epoch, 1] = L_b
        out_profile[epoch, 2] = lambda_b
        out_profile[epoch, 3] = h_b
        out_profile[epoch, 4:7] = v_eb_n.transpose()
        out_profile[epoch, 7:10] = eul_nb.transpose()

    # Last (epoch
    out_profile[no_epochs-1, :] = in_profile[no_epochs-1, :]

    # Write output profile
    Write_profile(out_filename, out_profile)
