#!/usr/bin/python3

import numpy as np
import math

from utils import R_0, mu, J_2, omega_ie


def Gravity_ECEF(r_eb_e):
    # Gravitation_ECI - Calculates  acceleration due to gravity resolved about
    # ECEF-frame
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 1/4/2012 by Paul Groves
    #
    # Inputs:
    #   r_eb_e  Cartesian position of body frame w.r.t. ECEF frame, resolved
    #           about ECEF-frame axes (m)
    # Outputs:
    #   g       Acceleration due to gravity (m/s^2)

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Parameters
    # WGS84 Equatorial radius in meters
    R_0 = 6378137
    # WGS84 Earth gravitational constant (m^3 s^-2)
    mu = 3.986004418E14
    # WGS84 Earth's second gravitational constant
    J_2 = 1.082627E-3
    # Earth rotation rate (rad/s)
    omega_ie = 7.292115E-5

    # Begins

    # Calculate distance from center of the Earth
    mag_r = np.sqrt(r_eb_e.transpose() * r_eb_e)

    # If the input position is 0,0,0, produce a dummy output
    if mag_r == 0:
        g = np.matrix(np.zeros((3,1)))
    else:
        z_scale = 5 * math.pow((r_eb_e[2, 0] / mag_r), 2)
        gamma = -mu / math.pow(mag_r, 3) * (r_eb_e + 1.5 * J_2 * math.pow(R_0 / mag_r, 2) *
                                            np.matrix([(1.0 - z_scale) * r_eb_e[0, 0], (1.0 - z_scale) * r_eb_e[1, 0], (3.0 - z_scale) * r_eb_e[2, 0]]).transpose())

        # Add centripetal acceleration using (2.133)
        g = np.matrix(np.zeros((3,1)))
        g[0:2, 0] = gamma[0:2, 0] + omega_ie*omega_ie * r_eb_e[0:2, 0]
        g[2, 0] = gamma[2, 0]

    return g
