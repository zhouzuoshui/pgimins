#!/usr/bin/python3

import numpy as np


def CTM_to_Euler(C):
    # CTM_to_Euler - Converts a coordinate transformation matrix to the
    #corresponding set of Euler angles#
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 1/4/2012 by Paul Groves
    #
    # Inputs:
    #   C       coordinate transformation matrix describing transformation from
    #           beta to alpha, in form of numpy.matrix
    #
    # Outputs:
    #   eul     Euler angles describing rotation from beta to alpha in the
    #           order roll, pitch, yaw(rad), in form of numpy.matrix

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    # Calculate Euler angles using (2.23)
    eul = np.matrix([0.0, 0.0, 0.0])

    eul[0, 0] = np.arctan2(C[1, 2], C[2, 2])  # roll
    eul[0, 1] = -np.arcsin(C[0, 2])           # pitch
    eul[0, 2] = np.arctan2(C[0, 1], C[0, 0])  # yaw

    return eul.transpose()
