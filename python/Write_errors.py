#!/usr/bin/python3

import numpy as np
import math
import csv
from utils import rad_to_deg, deg_to_rad

def Write_errors(filename, out_errors):
    # Write_errors - outputs the errors in the following .csv format
    # Column 1: time (sec)
    # Column 2: north position error (m)
    # Column 3: east position error (m)
    # Column 4: down position error (m)
    # Column 5: north velocity (m/s)
    # Column 6: east velocity (m/s)
    # Column 7: down velocity (m/s)
    # Column 8: roll component of NED attitude error (deg)
    # Column 9: pitch component of NED attitude error (deg)
    # Column 10: yaw component of NED attitude error (deg)
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 31/3/2012 by Paul Groves
    #
    # Inputs:
    #   filename     Name of file to write
    #   out_errors   Array of data to write

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    # Convert attitude errors from radians to degrees
    for j in [7, 8, 9]:
        out_errors[:, j] = rad_to_deg * out_errors[:, j]

    # Write output profile # Windows上的NL符号，12位精度
    # dlmwrite(filename,out_profile,'newline','pc','precision',12)

    with open(filename, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(np.array(out_errors))
