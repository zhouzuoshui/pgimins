#!/usr/bin/python3

import numpy as np
import math

from utils import R_0, e, ecc2


def ECEF_to_NED(r_eb_e, v_eb_e, C_b_e):
    # ECEF_to_NED - Converts Cartesian  to curvilinear position, velocity
    # resolving axes from ECEF to NED and attitude from ECEF- to NED-referenced
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 2/4/2012 by Paul Groves
    #
    # Inputs:
    #   r_eb_e        Cartesian position of body frame w.r.t. ECEF frame, resolved
    #                 along ECEF-frame axes (m), in form of np.matrix
    #   v_eb_e        velocity of body frame w.r.t. ECEF frame, resolved along
    #                 ECEF-frame axes (m/s)
    #   C_b_e         body-to-ECEF-frame coordinate transformation matrix
    #
    # Outputs:
    #   L_b           latitude (rad)
    #   lambda_b      longitude (rad)
    #   h_b           height (m)
    #   v_eb_n        velocity of body frame w.r.t. ECEF frame, resolved along
    #                 north, east, and down (m/s)
    #   C_b_n         body-to-NED coordinate transformation matrix
    
    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    # Convert position using Borkowski closed-form exact solution
    # From (2.113)
    lambda_b = np.arctan2(r_eb_e[1, 0], r_eb_e[0, 0])

    # From (C.29) and (C.30)
    k1 = np.sqrt(1 - ecc2) * np.abs(r_eb_e[2, 0])
    k2 = ecc2 * R_0
    beta = math.hypot(r_eb_e[1, 0], r_eb_e[0, 0])
    E = (k1 - k2) / beta
    F = (k1 + k2) / beta

    # From (C.31)
    P = 4/3 * (E * F + 1)

    # From (C.32)
    Q = 2 * (E * E - F * F)

    # From (C.33)
    D = P*P*P + Q*Q

    # From (C.34)
    V = math.pow(np.sqrt(D) - Q, 1/3) - math.pow(np.sqrt(D) + Q, 1/3)

    # From (C.35)
    G = 0.5 * (np.sqrt(E * E + V) + E)

    # From (C.36)
    T = np.sqrt(G * G + (F - V * G) / (2 * G - E)) - G

    # From (C.37)
    L_b = np.sign(r_eb_e[2,0]) * np.arctan((1 - T * T) /
                                         (2 * T * np.sqrt(1 - ecc2)))

    # From (C.38)
    h_b = (beta - R_0 * T) * np.cos(L_b) + \
        (r_eb_e[2, 0] - np.sign(r_eb_e[2, 0]) *
         R_0 * np.sqrt(1 - ecc2)) * np.sin(L_b)

    # Calculate ECEF to NED coordinate transformation matrix using (2.150)
    cos_lat = np.cos(L_b)
    sin_lat = np.sin(L_b)
    cos_long = np.cos(lambda_b)
    sin_long = np.sin(lambda_b)
    C_e_n = np.matrix([[-sin_lat * cos_long, -sin_lat * sin_long,  cos_lat],
                       [-sin_long,            cos_long,        0.0],
                       [-cos_lat * cos_long, -cos_lat * sin_long, -sin_lat]])

    # Transform velocity using (2.73)
    v_eb_n = C_e_n * v_eb_e

    # Transform attitude using (2.15)
    C_b_n = C_e_n * C_b_e

    return L_b, lambda_b, h_b, v_eb_n, C_b_n
