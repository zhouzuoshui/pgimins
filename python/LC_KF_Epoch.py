#!/usr/bin/python3

import numpy as np
import math

from Gravity_ECEF import *
from Gravitation_ECI import *
from Skew_symmetric import *
from utils import c, omega_ie, R_0, e


def LC_KF_Epoch(GNSS_r_eb_e, GNSS_v_eb_e, tor_s, est_C_b_e_old, est_v_eb_e_old, est_r_eb_e_old, est_IMU_bias_old,
                P_matrix_old, meas_f_ib_b, est_L_b_old, LC_KF_config):
    # LC_KF_Epoch - Implements one cycle of the loosely coupled INS/GNSS
    # Kalman filter plus closed-loop correction of all inertial states
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 12/4/2012 by Paul Groves
    #
    # Inputs:
    #   GNSS_r_eb_e           GNSS estimated ECEF user position (m)
    #   GNSS_v_eb_e           GNSS estimated ECEF user velocity (m/s)
    #   tor_s                 propagation interval (s)
    #   est_C_b_e_old         prior estimated body to ECEF coordinate
    #                         transformation matrix
    #   est_v_eb_e_old        prior estimated ECEF user velocity (m/s)
    #   est_r_eb_e_old        prior estimated ECEF user position (m)
    #   est_IMU_bias_old      prior estimated IMU biases (body axes)
    #   P_matrix_old          previous Kalman filter error covariance matrix
    #   meas_f_ib_b           measured specific force
    #   est_L_b_old           previous latitude solution
    #   LC_KF_config
    #     .gyro_noise_PSD     Gyro noise PSD (rad^2/s)
    #     .accel_noise_PSD    Accelerometer noise PSD (m^2 s^-3)
    #     .accel_bias_PSD     Accelerometer bias random walk PSD (m^2 s^-5)
    #     .gyro_bias_PSD      Gyro bias random walk PSD (rad^2 s^-3)
    #     .pos_meas_SD            Position measurement noise SD per axis (m)
    #     .vel_meas_SD            Velocity measurement noise SD per axis (m/s)
    #
    # Outputs:
    #   est_C_b_e_new     updated estimated body to ECEF coordinate
    #                      transformation matrix
    #   est_v_eb_e_new    updated estimated ECEF user velocity (m/s)
    #   est_r_eb_e_new    updated estimated ECEF user position (m)
    #   est_IMU_bias_new  updated estimated IMU biases
    #     Rows 1-3          estimated accelerometer biases (m/s^2)
    #     Rows 4-6          estimated gyro biases (rad/s)
    #   P_matrix_new      updated Kalman filter error covariance matrix

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    # Skew symmetric matrix of Earth rate
    Omega_ie = Skew_symmetric(np.matrix([0.0, 0.0, omega_ie]).transpose())

    # SYSTEM PROPAGATION PHASE

    # 1. Determine transition matrix using (14.50) (first-order approx)
    Phi_matrix = np.matrix(np.eye(15))
    Phi_matrix[0:3, 0:3] = Phi_matrix[0:3, 0:3] - Omega_ie * tor_s
    Phi_matrix[0:3, 12:15] = est_C_b_e_old * tor_s
    Phi_matrix[3:6, 0:3] = -tor_s * Skew_symmetric(est_C_b_e_old * meas_f_ib_b)
    Phi_matrix[3:6, 3:6] = Phi_matrix[3:6, 3:6] - 2 * Omega_ie * tor_s
    geocentric_radius = R_0 / math.sqrt(1 - math.pow((e * np.sin(est_L_b_old)), 2)) * \
        math.sqrt(math.pow(np.cos(est_L_b_old), 2) + math.pow((1 - e*e), 2)
                  * math.pow(np.sin(est_L_b_old), 2))  # from (2.137)
    Phi_matrix[3:6, 6:9] = -tor_s * 2 * Gravity_ECEF(est_r_eb_e_old) / \
        geocentric_radius * est_r_eb_e_old.transpose() / \
        math.sqrt(est_r_eb_e_old.transpose() * est_r_eb_e_old)
    Phi_matrix[3:6, 9:12] = est_C_b_e_old * tor_s
    Phi_matrix[6:9, 3:6] = np.matrix(np.eye(3)) * tor_s

    # 2. Determine approximate system noise covariance matrix using (14.82)
    Q_prime_matrix = np.matrix(np.zeros((15, 15)))
    Q_prime_matrix[0:3, 0:3] = np.matrix(
        np.eye(3)) * LC_KF_config.gyro_noise_PSD * tor_s
    Q_prime_matrix[3:6, 3:6] = np.matrix(
        np.eye(3)) * LC_KF_config.accel_noise_PSD * tor_s
    Q_prime_matrix[9:12, 9:12] = np.matrix(
        np.eye(3)) * LC_KF_config.accel_bias_PSD * tor_s
    Q_prime_matrix[12:15, 12:15] = np.matrix(
        np.eye(3)) * LC_KF_config.gyro_bias_PSD * tor_s

    # 3. Propagate state estimates using (3.14) noting that all states are zero
    # due to closed-loop correction.
    x_est_propagated = np.matrix(np.zeros((15, 1)))

    # 4. Propagate state estimation error covariance matrix using (3.46)
    P_matrix_propagated = Phi_matrix * \
        (P_matrix_old + 0.5 * Q_prime_matrix) * \
        Phi_matrix.transpose() + 0.5 * Q_prime_matrix

    # MEASUREMENT UPDATE PHASE

    # 5. Set-up measurement matrix using (14.115)
    H_matrix = np.matrix(np.zeros((6, 15)))
    H_matrix[0:3, 6:9] = -np.matrix(np.eye(3))
    H_matrix[3:6, 3:6] = -np.matrix(np.eye(3))

    # 6. Set-up measurement noise covariance matrix assuming all components of
    # GNSS position and velocity are independent and have equal variance.
    R_matrix = np.matrix(np.zeros((6, 6)))
    R_matrix[0:3, 0:3] = np.matrix(
        np.eye(3)) * LC_KF_config.pos_meas_SD * LC_KF_config.pos_meas_SD
    R_matrix[0:3, 3:6] = np.matrix(np.zeros((3, 3)))
    R_matrix[3:6, 0:3] = np.matrix(np.zeros((3, 3)))
    R_matrix[3:6, 3:6] = np.matrix(
        np.eye(3)) * LC_KF_config.vel_meas_SD * LC_KF_config.vel_meas_SD

    # 7. Calculate Kalman gain using (3.21)
    K_matrix = P_matrix_propagated * H_matrix.transpose() * np.linalg.inv(H_matrix *
                                                                          P_matrix_propagated * H_matrix.transpose() + R_matrix)

    # 8. Formulate measurement innovations using (14.102), noting that zero
    # lever arm is assumed here
    delta_z = np.matrix(np.zeros((6, 1)))
    delta_z[0:3, 0] = GNSS_r_eb_e - est_r_eb_e_old
    delta_z[3:6, 0] = GNSS_v_eb_e - est_v_eb_e_old

    # 9. Update state estimates using (3.24)
    x_est_new = x_est_propagated + K_matrix * delta_z

    # 10. Update state estimation error covariance matrix using (3.25)
    P_matrix_new = (np.matrix(np.eye(15)) - K_matrix *
                    H_matrix) * P_matrix_propagated

    # CLOSED-LOOP CORRECTION

    # Correct attitude, velocity, and position using (14.7-9)
    est_C_b_e_new = (np.matrix(np.eye(3)) -
                     Skew_symmetric(x_est_new[0:3, 0])) * est_C_b_e_old
    est_v_eb_e_new = est_v_eb_e_old - x_est_new[3:6, 0]
    est_r_eb_e_new = est_r_eb_e_old - x_est_new[6:9, 0]

    # Update IMU bias estimates
    est_IMU_bias_new = est_IMU_bias_old + x_est_new[9:15, 0]

    return est_C_b_e_new, est_v_eb_e_new, est_r_eb_e_new, est_IMU_bias_new, P_matrix_new
