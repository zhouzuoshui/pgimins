#!/usr/bin/python3

import numpy as np
import math

from utils import R_0, mu, J_2


def Gravitation_ECI(r_ib_i):
    # Gravitation_ECI - Calculates gravitational acceleration resolved about
    # ECI-frame axes
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.

    # This function created 1/4/2012 by Paul Groves
    #
    # Inputs:
    #   r_ib_i  Cartesian position of body frame w.r.t. ECI frame, resolved
    #           about ECI-frame axes (m)
    # Outputs:
    #   gamma   Acceleration due to gravitational force (m/s^2)

    # Copyright 2012, Paul Groves
    # Ported to Python by Zuoshui Zhou
    # License: BSD; see license.txt for details
    
    # Begins

    # Calculate distance from center of the Earth
    mag_r = math.sqrt(r_ib_i.transpose() * r_ib_i)

    # If the input position is 0,0,0, produce a dummy output
    gamma = np.matrix(np.zeros((3,1)))

    # Calculate gravitational acceleration using (2.141)
    if mag_r != 0:
        z_scale = 5 * math.pow(r_ib_i[2, 0] / mag_r, 2)
        gamma = -mu / math.pow(mag_r, 3) * (r_ib_i + 1.5 * J_2 * math.pow(R_0 / mag_r, 2) * np.matrix([[(1.0 - z_scale) * r_ib_i[0, 0]],
                                                                                                       [(1.0 - z_scale) * r_ib_i[1, 0]],
                                                                                                       [(3.0 - z_scale) * r_ib_i[2, 0]]]))

    return gamma
