#!/usr/bin/python3

import numpy as np
import math


def Initialize_LC_P_matrix(LC_KF_config):
    # Initialize_LC_P_matrix - Initializes the loosely coupled INS/GNSS KF
    # error covariance matrix
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 12/4/2012 by Paul Groves
    #
    # Inputs:
    #   TC_KF_config
    #     .init_att_unc           Initial attitude uncertainty per axis (rad)
    #     .init_vel_unc           Initial velocity uncertainty per axis (m/s)
    #     .init_pos_unc           Initial position uncertainty per axis (m)
    #     .init_b_a_unc           Initial accel. bias uncertainty (m/s^2)
    #     .init_b_g_unc           Initial gyro. bias uncertainty (rad/s)
    #
    # Outputs:
    #   P_matrix              state estimation error covariance matrix

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    # Initialize error covariance matrix
    P_matrix = np.matrix(np.zeros((15, 15)))

    P_matrix[0:3, 0:3] = np.matrix(
        np.eye(3)) * math.pow(LC_KF_config.init_att_unc, 2)
    P_matrix[3:6, 3:6] = np.matrix(
        np.eye(3)) * math.pow(LC_KF_config.init_vel_unc, 2)
    P_matrix[6:9, 6:9] = np.matrix(
        np.eye(3)) * math.pow(LC_KF_config.init_pos_unc, 2)
    P_matrix[9:12, 9:12] = np.matrix(
        np.eye(3)) * math.pow(LC_KF_config.init_b_a_unc, 2)
    P_matrix[12:15, 12:15] = np.matrix(
        np.eye(3)) * math.pow(LC_KF_config.init_b_g_unc, 2)

    return P_matrix
