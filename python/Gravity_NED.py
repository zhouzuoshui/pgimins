#!/usr/bin/python3

import numpy as np
import math

from utils import R_0, R_P, e, omega_ie, mu, f

def Gravity_NED(L_b, h_b):
    # Gravity_ECEF - Calculates  acceleration due to gravity resolved about
    # north, east, and down
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 2/4/2012 by Paul Groves
    #
    # Inputs:
    #   L_b           latitude (rad)
    #   h_b           height (m)
    # Outputs:
    #   g       Acceleration due to gravity (m/s^2)

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Parameters


    # Begins

    # Calculate surface gravity using the Somigliana model, (2.134)
    sinsqL = np.sin(L_b) * np.sin(L_b)
    g_0 = 9.7803253359 * (1 + 0.001931853 * sinsqL) / \
        math.sqrt(1 - e * e * sinsqL)

    # Calculate north gravity using (2.140)
    gnorth = -8.08E-9 * h_b * np.sin(2 * L_b)

    # East gravity is zero
    geast = 0

    # Calculate down gravity using (2.139)
    gdown = g_0 * (1 - (2 / R_0) * (1 + f * (1 - 2 * sinsqL) + (omega_ie *
                                                                  omega_ie * R_0 * R_0 * R_P / mu)) * h_b + (3 * h_b * h_b / R_0 / R_0))
    
    g = np.matrix([gnorth, geast, gdown], dtype=np.float64).transpose()
    return g
